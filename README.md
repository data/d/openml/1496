# OpenML dataset: ringnorm

https://www.openml.org/d/1496

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Michael Revow     
**Source**: http://www.cs.toronto.edu/~delve/data/ringnorm/desc.html   
**Please cite**:     

1: Abstract: 

This is a 20 dimensional, 2 class classification problem. Each class is drawn from a multivariate normal distribution. Class 1 has mean zero and covariance 4 times the identity. Class 2 has mean (a,a,..a) and unit covariance. a = 2/sqrt(20). 

2: Data set description.

This is an implementation of Leo Breiman's ringnorm example[1]. It is a 20 dimensional, 2 class classification example. Each class is drawn from a multivariate normal distribution. Class 1 has mean zero and covariance 4 times the identity. Class 2 has mean (a,a,..a) and unit covariance. a = 2/sqrt(20). Breiman reports the theoretical expected misclassification rate as 1.3%. He used 300 training examples with CART and found an error of 21.4%.


- Type.          Classification 
- Origin.  Laboratory
- Instances.  7400
- Features.  20
- Classes.  2 
- Missing values. No

3: Attributes information

@relation ring
@attribute A1 real [-6879.0, 6285.0]
@attribute A2 real [-7141.0, 6921.0]
@attribute A3 real [-7734.0, 7611.0]
@attribute A4 real [-6627.0, 7149.0]
@attribute A5 real [-7184.0, 6383.0]
@attribute A6 real [-6946.0, 6743.0]
@attribute A7 real [-7781.0, 6285.0]
@attribute A8 real [-6882.0, 6357.0]
@attribute A9 real [-7184.0, 7487.0]
@attribute A10 real [-7232.0, 6757.0]
@attribute A11 real [-7803.0, 7208.0]
@attribute A12 real [-7395.0, 6791.0]
@attribute A13 real [-7096.0, 6403.0]
@attribute A14 real [-7472.0, 7261.0]
@attribute A15 real [-7342.0, 7372.0]
@attribute A16 real [-7121.0, 6905.0]
@attribute A17 real [-7163.0, 7175.0]
@attribute A18 real [-8778.0, 6896.0]
@attribute A19 real [-7554.0, 5726.0]
@attribute A20 real [-6722.0, 7627.0]
@attribute Class {0, 1}
@inputs A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, A16, A17, A18, A19, A20
@outputs Class

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1496) of an [OpenML dataset](https://www.openml.org/d/1496). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1496/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1496/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1496/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

